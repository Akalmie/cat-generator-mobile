# Project Title
Cat Generator


## Table of Content:

- [Project Title](#project-title)
  - [Table of Content:](#table-of-content)
  - [About The App](#about-the-app)
  - [Technologies](#technologies)
  - [Setup](#setup)
  - [Status](#status)


## About The App
[CAT GENERATOR] is an app made with React Native. I just wanna test this technologie. The app fetch cats photos and print them to the screen


## Technologies
I used `React Native`, `javascript`

## Setup
- download or clone the repository
- if you don't have React Native, run `npm install -g expo-cli`
- run `npm start` or `yarn start`


## Status
[Cat Generator] is done I think. Maybe i'll work another version one day.
