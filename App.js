import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, Button, Image } from 'react-native';
import { useState, useEffect } from 'react';

export default function App() {

  const [img, setimg] = useState('https://purr.objects-us-east-1.dream.io/i/2015-02-1716.10.07-2.jpg');

  const getCat = () => {
    fetch('https://aws.random.cat/meow')
      .then((res) => {
        return res.json()
      }).then((data) => {
        setimg(data.file)
        console.log(img);

      });
  }

  useEffect(() => {
    getCat()
  }, [])

  return (
    <View style={styles.container}>
      <Image
        source={{ uri: img }}
        style={styles.img}
      />
      <Button
        onPress={getCat}
        title="New cat"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: '80%',
    height: '65%',
    marginBottom: '5%'
  }
});
